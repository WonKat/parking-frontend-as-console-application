using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Parking.Abstractions;
using Parking.Exceptions;
using Parking.Models;


namespace Parking.Implementation
{
    public class Parking:IParking
    {
        private const string ServerAdress = "http://localhost:5000/api/";
        public void AddNewTransport(Transport transport)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(ServerAdress + "parking/transport"),
                    Method = HttpMethod.Post,
                    Content  = new StringContent(JsonConvert.SerializeObject(transport),Encoding.Unicode,MediaTypeNames.Application.Json)
                };
                var response = client.SendAsync(request).Result;
                if(!response.IsSuccessStatusCode)
                    throw new BadRequestException(JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result));                
            }
        }

        public List<Transport> GetAllAvailableTransport()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(ServerAdress + "parking/transport"),
                    Method = HttpMethod.Get
                };
                var response = client.SendAsync(request).Result;
                return JsonConvert.DeserializeObject<List<Transport>>(response.Content.ReadAsStringAsync().Result);
            }
        }

        public List<Transaction> GetAllTransactionsForLastMinute()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(ServerAdress + "parking/transactions/last"),
                    Method = HttpMethod.Get
                };
                var response = client.SendAsync(request).Result;
                return JsonConvert.DeserializeObject<List<Transaction>>(response.Content.ReadAsStringAsync().Result);
            }
        }

        public List<string> GetAllTransactionsHistory()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(ServerAdress + "parking/transactions"),
                    Method = HttpMethod.Get
                };
                var response = client.SendAsync(request).Result;
                if(response.StatusCode == HttpStatusCode.NoContent)
                    throw  new FileNotFoundException();
                return JsonConvert.DeserializeObject<List<string>>(response.Content.ReadAsStringAsync().Result);
            }
        }

        public Tuple<int, int> GetInforationAboutFreeOccupiedPlaces()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(ServerAdress + "parking/info/places"),
                    Method = HttpMethod.Get
                };
                var response = client.SendAsync(request).Result;
                return JsonConvert.DeserializeObject<Tuple<int,int>>(response.Content.ReadAsStringAsync().Result);
            }
        }
        public double GetSalaryForLastMinute()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(ServerAdress + "parking/transactions/last/salary"),
                    Method = HttpMethod.Get
                };
                var response = client.SendAsync(request).Result;
                return JsonConvert.DeserializeObject<double>(response.Content.ReadAsStringAsync().Result);
            }
        }

 
        public bool IsTransportWithSpecificIdOlreadyExist(int Id)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(ServerAdress + $"parking/transport/exist?id={Id}"),
                    Method = HttpMethod.Get,
                };
                var response = client.SendAsync(request).Result;
                return response.StatusCode == HttpStatusCode.OK;
            }
        }
      
        public void RemoveExistTransport(int transportId)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(ServerAdress + $"parking/transport?id={transportId}"),
                    Method = HttpMethod.Delete,
                };
                var response = client.SendAsync(request).Result;
                if(response.StatusCode == HttpStatusCode.NoContent)
                    throw new FullParkingException();
                if(response.StatusCode == HttpStatusCode.BadRequest)
                    throw new BadRequestException();
            }
        }

        public void ReplenishBalanceForExistTransport(int transportId, int sum)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(ServerAdress + $"parking/transport?id={transportId}&sum={sum}"),
                    Method = HttpMethod.Put,
                };
                var response = client.SendAsync(request).Result;
                if (response.StatusCode == HttpStatusCode.NoContent)
                    throw new FullParkingException();
                if (response.StatusCode == HttpStatusCode.BadRequest)
                    throw new BadRequestException();
            }
        }

       
        public void SetConfigurations(double startBalance, int maxParkingSpaciousness, double periodForPaymentsInSeconds, double taxCoefficient)
        {
            var body = new 
            {
                MaxParkingSpaciousness = maxParkingSpaciousness,
                PeriodForPaymentsInSeconds = periodForPaymentsInSeconds,
                StartParkingBalance = startBalance,
                TaxCoefficient = taxCoefficient
            };
            var bodyJson = JsonConvert.SerializeObject(body);
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(ServerAdress + "configuration/set"),
                    Method = HttpMethod.Post,
                    Content = new StringContent(bodyJson,Encoding.UTF8,MediaTypeNames.Application.Json)
                };
                client.SendAsync(request).Wait();
            }
        }

        public void ResetTimers()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(ServerAdress + "configuration/timers"),
                    Method = HttpMethod.Delete
                };
                client.SendAsync(request).Wait();
            }
        }

        public double GetCurrentBalance()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(ServerAdress + "parking/info/balance"),
                    Method = HttpMethod.Get
                };
                var response = client.SendAsync(request).Result;
                return JsonConvert.DeserializeObject<double>(response.Content.ReadAsStringAsync().Result);
            }
        }
    }
}