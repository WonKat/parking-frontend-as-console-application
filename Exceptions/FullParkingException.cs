﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Exceptions
{
    class FullParkingException:Exception
    {
        public FullParkingException()
        {
            
        }
        public FullParkingException(string message):base(message)
        {
            
        }
    }
}
