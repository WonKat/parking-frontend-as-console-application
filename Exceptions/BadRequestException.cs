﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Exceptions
{
    class BadRequestException:Exception
    {
        public BadRequestException()
        {
            
        }
        public BadRequestException(string message):base(message)
        {
            
        }
    }
}
