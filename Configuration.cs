using System;
using System.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using Parking.Abstractions;

namespace Parking
{
    public static class Configuration
    {
        private static readonly IServiceCollection Services = new ServiceCollection();
        public static double StartParkingBalance { get; set; } = 0;
        public static int MaxParkingSpaciousness { get; set; } = 10;
        public static double PeriodForPaymentsInSeconds { get; set; } = 5;
        public static double TaxCoefficient { get; set; } = 2.5;


        static Configuration()
        {

        }
        public static IServiceProvider ConfigureServices()
        {
            Services.AddSingleton<IParking,Implementation.Parking>();
            return Services.BuildServiceProvider();
        }
    }
}