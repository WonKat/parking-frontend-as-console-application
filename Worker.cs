﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Parking.Abstractions;
using Parking.Exceptions;
using Parking.Models;

namespace Parking
{
    class Worker
    {
        private readonly IParking _parking;
        public Worker(IParking parking)
        {
            _parking = parking;
        }

        public void StartParking()
        {
            Console.CancelKeyPress += Console_CancelKeyPress;
            try
            {
                SetConfiguration();
                UserInteractionWithMenu();
            }
            catch (AggregateException)
            {
                Console.WriteLine("Server is down(");
                ClearConsole();
            }
        }

        private void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            const string path = "./Transactions.log";
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        public void UserInteractionWithMenu()
        {
            bool exitFromProgramm = false;
            while (!exitFromProgramm)
            {

                Console.WriteLine("\t\tParking menu");
                Console.WriteLine("1) Show salary for last minute");
                Console.WriteLine("2) Show current parking balance");
                Console.WriteLine("3) Show number of free/occupied parking spaces");
                Console.WriteLine("4) Show transactions for last minute");
                Console.WriteLine("5) Show transaction history");
                Console.WriteLine("6) Show list of available transport");
                Console.WriteLine("7) Add new transport");
                Console.WriteLine("8) Remove exist transport");
                Console.WriteLine("9) Replenish balance for specific transport");
                Console.WriteLine("0) Exit");
                Console.WriteLine("\n");
                Console.Write("Your input: ");

                var rightInput = int.TryParse(Console.ReadLine(), out int input);
                switch (input)
                {
                    case 1:
                    {
                        Console.WriteLine($"Salary for last minute : {_parking.GetSalaryForLastMinute()}");
                        break;
                    }
                    case 2:
                    {
                        Console.WriteLine($"Current parking balance is : {_parking.GetCurrentBalance()}");
                        break;
                    }
                    case 3:
                    {
                        var freeOccuplied = _parking.GetInforationAboutFreeOccupiedPlaces();
                        Console.WriteLine($"{freeOccuplied.Item1} places are free and {freeOccuplied.Item2} places are occupied");
                        break;
                    }
                    case 4:
                    {                        
                        foreach (var transaction in _parking.GetAllTransactionsForLastMinute())
                        {
                            Console.WriteLine(transaction.ToString());
                        }
                        break;
                    }
                    case 5:
                    {
                        try
                        {
                            foreach (var transaction in _parking.GetAllTransactionsHistory())
                            {
                                Console.WriteLine(transaction);
                            }
                        }
                        catch (FileNotFoundException)
                        {
                            Console.WriteLine("Journal with transactions is empty");                            
                        }
                            
                        break;
                    }
                    case 6:
                    {
                        foreach (var transport in _parking.GetAllAvailableTransport())
                        {
                            Console.WriteLine(transport.ToString());
                        }

                        break;
                    }
                    case 7:
                    {
                        UserInputToCreatenewTransport();
                        break;
                    }
                    case 8:
                    {
                        try
                        {
                            _parking.RemoveExistTransport(
                                ValidateIntInput("Input transport Id which you wanna remove"));
                        }
                        catch (BadRequestException)
                        {
                            Console.WriteLine("Parking haven't transport with same id");
                        }
                        catch (FullParkingException)
                        {
                            Console.WriteLine("Parking is empty(");
                        }
                        break;
                    }
                    case 9:
                    {
                        try
                        {
                            _parking.ReplenishBalanceForExistTransport(
                                ValidateIntInput("Input transport Id which u wanna replenish"),
                                ValidateIntInput("Input sum for replenish "));
                        }
                        catch (BadRequestException)
                        {
                            Console.WriteLine("Parking haven't transport with same id");
                        }
                        catch (FullParkingException)
                        {
                            Console.WriteLine("Parking is empty(");
                        }

                       break;
                    }
                    case 0:
                    {
                        exitFromProgramm = true;
                        Console.WriteLine("Good bye");
                        break;
                    }
                    default:
                    {
                        Console.WriteLine("Invalid input please input number 1-8 or 0");
                        break;
                    }
                }
                ClearConsole();
            }
        }

        private void UserInputToCreatenewTransport()
        {
            Console.WriteLine("So we create new transport");
            var type = ValidateTypeInput(
                "Select what type transport will be ([1] - Car, [2] - Truck, [3] - Bus, [4] - Motorcicle)");      
            var transport = new Transport(type)
            {
                Id= ValidateIdInput("Input unique Id for new transport"),
                Balance = ValidateDoubleInput("Input start balance")
            };
            Console.WriteLine("Input driver name");
            transport.DriverName = Console.ReadLine();            
            try
            {
                _parking.AddNewTransport(transport);
            }
            catch (BadRequestException e)
            {
                Console.WriteLine(e.Message);
            }
        }
       
        private void SetConfiguration()
        {
            Console.WriteLine("Welcome to the parking");
            Console.WriteLine("Start with default values " +
                              "(input [no] if u don't wanna use default values)");
            var overrideDefaultValues = Console.ReadLine().ToLower().Equals("no");
            if (overrideDefaultValues)
            {
                Console.WriteLine("Ok, seems like u wanna override default values");
                Configuration.StartParkingBalance = ValidateDoubleInput("Input start parking money balance");
                Configuration.MaxParkingSpaciousness = ValidateIntInput("Input number of max available parking places");
                Configuration.PeriodForPaymentsInSeconds = ValidateDoubleInput("Input the payment period (in seconds)");
                Configuration.TaxCoefficient = ValidateDoubleInput("Input tax coefficient for parking");
            }

            _parking.SetConfigurations(Configuration.StartParkingBalance, Configuration.MaxParkingSpaciousness, 
                Configuration.PeriodForPaymentsInSeconds, Configuration.TaxCoefficient);
            ClearConsole();
        }

        private int ValidateIdInput(string parameterDescription)
        {
            Console.WriteLine(parameterDescription);
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out int input) &&
                    !_parking.IsTransportWithSpecificIdOlreadyExist(input))
                {
                    return input;
                }
                Console.WriteLine("Invalid input you must input numeric value >= 1 or transport with same Id already exist, try one more time");
            }
        }
        private TransportTypes ValidateTypeInput(string parameterDescription)
        {
            Console.WriteLine(parameterDescription);
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out int input) &&
                    input >= 1 && input <=4)
                {
                    switch (input)
                    {
                        case 1:
                            return TransportTypes.Car;
                        case 2:
                            return TransportTypes.Truck;
                        case 3:
                            return TransportTypes.Bus;
                        case 4:
                            return TransportTypes.Motorcycle;
                        default:
                            break;                        
                    }
                }
                Console.WriteLine("Invalid input you must input integer value >= 1 and <= 4, try one more time");
            }
        }
        private int ValidateIntInput(string parameterDescription)
        {
            Console.WriteLine(parameterDescription);

            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out int input) && 
                    input >= 1)
                {
                    return input;
                }
                Console.WriteLine("Invalid input you must input integer value >= 1, try one more time");
            }
        }
        private double ValidateDoubleInput(string parameterDescription)
        {
            Console.WriteLine(parameterDescription);
            while (true)
            {
                if (double.TryParse(Console.ReadLine(), out double input) &&
                    input>=1)
                {
                    return input;
                }
                Console.WriteLine("Invalid input you must input numeric value >= 1, try one more time");
            }
        }

        private void ClearConsole()
        {
            Console.Write("Press any key to continue ...");
            Console.ReadKey(true);
            Console.Clear();
        }
    }
}
