using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Parking.Models;

namespace Parking.Abstractions
{
    public interface IParking
    {
        void SetConfigurations(double startBalance, int maxParkingSpaciousness, double periodForPaymentsInSeconds,
            double taxCoefficient);    
        void ResetTimers();
        Tuple<int, int> GetInforationAboutFreeOccupiedPlaces();
        List<Transaction> GetAllTransactionsForLastMinute();
        List<string> GetAllTransactionsHistory();
        List<Transport> GetAllAvailableTransport();
        void AddNewTransport(Transport transport);
        void RemoveExistTransport(int transportId);
        void ReplenishBalanceForExistTransport(int transportId, int sum);
        double GetSalaryForLastMinute();
        bool IsTransportWithSpecificIdOlreadyExist(int Id);
        double GetCurrentBalance();
    }
}