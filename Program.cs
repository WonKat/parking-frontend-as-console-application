﻿using System;
using System.Security.Authentication.ExtendedProtection;
using Microsoft.Extensions.DependencyInjection;
using Parking.Abstractions;

namespace Parking
{
    class Program
    {
        static void Main(string[] args)
        {
           var services = Configuration.ConfigureServices();
           var worker = new Worker(services.GetService<IParking>());
           worker.StartParking();
        }
    }
}
